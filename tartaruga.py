# -*- coding: utf-8 -*-

import time

import pyscreenshot as ImageGrab
from pymouse import PyMouse


#posizioni assolute
start_x, start_y = 288, 250
end_x, end_y = 1311, 1017
bbox = (start_x, start_y, end_x, end_y)


def grabba():
    print 'click'
    return ImageGrab.grab(bbox)


def somma(a, b):
    return a[0] + b[0], a[1] + b[1]


def sottrai(a, b):
    return a[0] - b[0], a[1] - b[1]


def dividi(a, quoziente):
    return a[0] / quoziente, a[1] / quoziente


def moltiplica(a, scalare):
    return a[0] * scalare, a[1] * scalare


def leggi_origine():
    m = PyMouse()
    data = []
    for i in range(0, 5):
        time.sleep(1.0)
        data.append(m.position())
    return sum([d[0] for d in data]) / len(data), sum([d[1] for d in data]) / len(data)


def vai_lentamente(start, nuova_destinazione):
    m = PyMouse()
    print "from", start, "to", nuova_destinazione
    nr = 10
    step = dividi(sottrai(nuova_destinazione, start), nr)
    for i in range(nr):
        partial_target = somma(start, moltiplica(step, i))
        m.move(*partial_target)
        print "move", partial_target
        time.sleep(0.5)


def clicca(posizione):
    m = PyMouse()
    m.press(*posizione)
    print "click", posizione
    time.sleep(0.2)
    m.release(*posizione)


def inizializza():
    print "partito"
    m = PyMouse()
    time.sleep(3.0)
    print "posizionati e spingi in alto"
    for i in range(15):
        print m.position()
        time.sleep(1.0)
    print "adesso fermo"
    origine = leggi_origine()
    print origine
    return origine


class Positioner(object):
    def __init__(self, origine):
        self.origine = origine
        self.offsets = {
            'quick_fight': (710, 666),
            'via': (506, 399),
            'claim': (507, 579),
            'ok': (551, 683),
            'scatola_1': (109, 274),
            'scatola_2': (265, 275),
            'scatola_3': (119, 500),
            'scatola_4': (287, 495),
            'orologio_1': (49, 183),
            'orologio_2': (227, 183),
            'orologio_3': (49, 378),
            'orologio_4': (227, 378)}

    def getoffset(self, key):
        return self.offsets[key]
        
    def __getitem__(self, name):
        return somma(self.origine, self.offsets[name])

    
def combatti(origine):
    posi = Positioner(origine)
    cnt = 0
    while 1:
        vai_lentamente(origine, posi['quick_fight'])
        clicca(posi['quick_fight'])

        vai_lentamente(posi['quick_fight'], posi['via'])
        grabba().save('data/%09d_pre.jpg'%cnt)
        clicca(posi['via'])

        print "guardo l'incontro"
        time.sleep(10.0)

        vai_lentamente(posi['via'], posi['claim'])
        clicca(posi['claim'])

        vai_lentamente(posi['claim'], posi['ok'])
        grabba().save('data/%09d_post.jpg'%cnt)
        clicca(posi['ok'])

        time.sleep(3.0)

        origine = posi['ok']
        cnt += 1

        
def scatola_in_apertura(im):
    color = (70, 109, 198)
    posi = Positioner((0, 0))
    for i in range(1, 5):
        if im.getpixel(posi.getoffset('orologio_%d'%i)) == color:
            print 'trovato:', i
            return i
        print i, im.getpixel(posi.getoffset('orologio_%d'%i))
    return -1


def apri_scatola(scatola, origine):
    posi = Positioner(origine)
    m = PyMouse()
    posizione_attuale = m.position()
    vai_lentamente(posizione_attuale, posi['scatola_%d'%scatola])
    clicca(posi['scatola_%d'%scatola])


def guarda(origine):
    im = grabba()
    scatola = scatola_in_apertura(im)
    if scatola != -1:
        apri_scatola(scatola, origine)

        
if __name__ == '__main__':
    origine = inizializza()
    combatti(origine)
#    guarda(origine)
